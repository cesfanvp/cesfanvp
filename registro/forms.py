from django import forms
from .models import Paciente


class RegModelForm(forms.ModelForm):

    class Meta:
        model = Paciente
        fields = ["nombre", "rut", "edad_ingreso", "edad_actual"]
        exclude = ()

    #Validacion comentada para poder dejar el campo en blanco en el template y en la base de datos guarde registro
    # def clean_nombre(self):
    #     nombre = self.cleaned_data.get("nombre")
    #     if nombre:
    #         if len(nombre) <= 5:
    #             raise forms.ValidationError("El campo nombre debe contener mas de cinco caracteres")
    #         return nombre
    #     else:
    #         raise forms.ValidationError("Debe ingresar al menos un caracter")

    def clean_edad_ingreso(self):
        min = self.cleaned_data.get("edad_ingreso")
        if min < 0:
            raise forms.ValidationError("La edad no puede ser inferior a 0")
        if min <= 17:
            return min
        else:
            raise forms.ValidationError("La edad ingresada no es valida ")

    def clean_edad_actual(self):
        min = self.cleaned_data.get("edad_actual")
        if min < 0:
            raise forms.ValidationError("La edad no puede ser inferior a 0")
        if min <= 17:
            return min
        else:
            raise forms.ValidationError("La edad ingresada no es valida")


class ContactForm(forms.Form):
    mensaje = forms.CharField(widget=forms.Textarea)
