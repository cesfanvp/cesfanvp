from django.db import models

# Create your models here.


class Paciente(models.Model):
    id = models.AutoField(primary_key=True, unique=True)
    nombre = models.CharField(max_length=80, blank=True, null=True)
    rut = models.CharField(max_length=12, blank=False, null=False)
    edad_ingreso = models.IntegerField()
    edad_actual = models.IntegerField()

    objects = models.Manager

    def __str__(self):
        return self.nombre