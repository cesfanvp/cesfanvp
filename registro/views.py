from django.shortcuts import render
from .forms import RegModelForm, ContactForm
from django.conf import settings
from django.core.mail import send_mail

# Create your views here.


def inicio_paciente(request):
    form = RegModelForm(request.POST or None)
    titulo = "BIENVENIDO AL CERTAMEN DE PYTHON NUMERO 2!"
    mensaje = "Datos Guardados Exitosamente!"

    if request.user.is_authenticated():
        titulo = "BIENVENIDO %s" % (request.user)

    contexto = {
        "el_formulario1": form,
        "el_titulo": titulo,
    }
    if form.is_valid():
        instance = form.save(commit=False)
        if not instance.nombre:
            instance.nombre = "SIN DATOS"
        instance.save()

        contexto = {
            "mensaje": mensaje,
        }

    return render(request, "paciente.html", contexto)


def contacto(request):
    form = ContactForm(request.POST or None)
    if form.is_valid():
        formulario_nombre = form.cleaned_data.get("nombre")
        formulario_email = form.cleaned_data.get("email")
        formulario_mensaje = form.cleaned_data.get("mensaje")
        asunto = "Formulario de contacto web"
        email_from = settings.EMAIL_HOST_USER
        email_to = [email_from, "jorgeborquez@udec.cl"]
        email_mensaje = "Enviado por %s – Correo: %s – Mensaje: %s" % (
        formulario_nombre, formulario_email, formulario_mensaje)
        send_mail(asunto, email_mensaje, email_from, email_to, fail_silently=False)

    contexto = {
        "el_contacto": form,
    }
    return render(request, "contacto.html", contexto)