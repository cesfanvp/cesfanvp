from django.contrib import admin
from .forms import *
# Register your models here.


class AdminPaciente(admin.ModelAdmin):
    list_display = ["nombre", "rut"]
    list_filter = ["nombre", "rut"]
    form = RegModelForm


admin.site.register(Paciente, AdminPaciente)