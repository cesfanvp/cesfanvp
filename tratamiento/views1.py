from django.shortcuts import render
from .forms import RegModelForm
# Create your views here.


def inicio_ficha(request):
    form = RegModelForm(request.POST or None)
    titulo = "BIENVENIDO AL CERTAMEN DE PYTHON NUMERO 2!"
    mensaje = "Datos Guardados Exitosamente!"

    if request.user.is_authenticated():
        titulo = "BIENVENIDO %s" % (request.user)

    contexto = {
        "el_formulario": form,
        "el_titulo": titulo,
    }
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()

        contexto = {
            "mensaje": mensaje,
        }

    return render(request, "ficha.html", contexto)

