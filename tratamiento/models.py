from django.db import models

# Create your models here.


class FichaClinica(models.Model):
    id = models.AutoField(primary_key=True, unique=True)
    fecha_ingreso_paciente = models.DateField()
    enfermedades = models.CharField(max_length=80, blank=False, null=False)
    tratamiento = models.CharField(max_length=80, blank=False, null=False)
    fecha_enfermedad = models.DateField()
    fecha_termino = models.DateField()
    resultado_tratamiento = models.CharField(max_length=80, blank=False, null=False)

    def __str__(self):
        return self.tratamiento
