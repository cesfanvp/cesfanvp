from django import forms
from .models import FichaClinica


class RegModelForm(forms.ModelForm):

    class Meta:
        model = FichaClinica
        fields = ["fecha_ingreso_paciente", "enfermedades", "tratamiento", "fecha_enfermedad", "fecha_termino", "resultado_tratamiento"]
        exclude = ()

    def clean_enfermedades(self):
        enfermedad = self.cleaned_data.get("enfermedades")
        if enfermedad:
            if len(enfermedad) <= 5:
                raise forms.ValidationError("El campo enfermedades debe contener mas de cinco caracteres")
            return enfermedad
        else:
            raise forms.ValidationError("Debe ingresar al menos un caracter")

    def clean_tratamiento(self):
        tratamiento = self.cleaned_data.get("tratamiento")
        if tratamiento:
            if len(tratamiento) <= 5:
                raise forms.ValidationError("El campo tratamiento debe contener mas de cinco caracteres")
            return tratamiento
        else:
            raise forms.ValidationError("Debe ingresar al menos un caracter")

    def clean_resultado_tratamiento(self):
        tratamiento = self.cleaned_data.get("resultado_tratamiento")
        if tratamiento:
            if len(tratamiento) <= 5:
                raise forms.ValidationError("El campo resultado_tratamiento debe contener mas de cinco caracteres")
            return tratamiento
        else:
            raise forms.ValidationError("Debe ingresar al menos un caracter")
