from django.contrib import admin
from .forms import *
# Register your models here.


class AdminFicha(admin.ModelAdmin):
    list_display = ["fecha_ingreso_paciente", "tratamiento"]
    list_filter = ["fecha_ingreso_paciente", "tratamiento"]
    form = RegModelForm


admin.site.register(FichaClinica, AdminFicha)